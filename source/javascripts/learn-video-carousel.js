$('.learn-video-slider').slick({
    dots: true,
    centerMode: false,
    infinite: true,
    speed: 1200,
    lazyLoad: 'ondemand',
    slidesToShow: 5,
    slidesToScroll: 5,
    arrows: true,
    nextArrow: '<div class="slick-arrow-next"><i class="fas fa-arrow-right"></i></div>',
    prevArrow: '<div class="slick-arrow-prev"><i class="fas fa-arrow-left"></i></div>',
    responsive: [{
            breakpoint: 1024,
            settings: {
                lazyLoad: 'ondemand',
                slidesToShow: 3,
                slidesToScroll: 3,
            }
        },
        {
            breakpoint: 600,
            settings: {
                lazyLoad: 'ondemand',
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                lazyLoad: 'ondemand',
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ]
});

$('#learnVideoCarouselModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var videoUrl = button.data('video-link')
    var modal = $(this)
    modal.find('.modal-title').text(videoUrl);
    modal.find('.share-text').text(videoUrl);
    modal.find('.carousel-modal-iframe').attr("src", videoUrl);
})

$('.btn-close-modal').click(function (e) {
    e.preventDefault();
    $('.carousel-modal-iframe').attr('src', '');
});
